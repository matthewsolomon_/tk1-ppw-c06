Nama-nama anggota Kelompok :
1. Raja Aldwyn James Napintor Sihombing (1906308066)
2. Qonita Nur Iffat (1906307233)
3. Haniif Rifki Ramadhan (1906306275)
4. Matthew Tumbur Parluhutan (1906308500)
5. Ikmal Almuhtadi Rajab (1906308324)


Link at : https://tk1-ppw-c06.herokuapp.com/

gitlab Repository : https://gitlab.com/matthewsolomon_/tk1-ppw-c06

prototype and wireframe : https://www.figma.com/files/team/897780161302135089/tk1-ppw-c06

Aplikasi yang diajukan : Aplikasi donasi dan cara-cara menghindari COVID. Aplikasi ini nantinya akan bermanfaat mengambil peran sebagai
suatu komunitas wadah dimana orang-orang dapat saling berdiskusi, mendapat pengetahuan akan cara menghindari COVID, mendapatkan informasi
seputar COVID, serta berdonasi.

Fitur yang akan diimplementasikan :
        - Fitur Login (Matthew)
        - Fitur Donasi (Raja)
        - Fitur News (Jumlah Pasien, dsb.) (Ikmal)
        - Fitur Forum (Bisa memberikan komentar) (Haniif)
        - Fitur halaman tips & tricks (Terdapat suatu halaman tips n tricks) (Qonita)


Pipeline Status : [![pipeline status](https://gitlab.com/matthewsolomon_/tk1-ppw-c06/badges/master/pipeline.svg)](https://gitlab.com/matthewsolomon_/tk1-ppw-c06/-/commits/master)