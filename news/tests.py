from django.test import TestCase, Client
from .models import komentar
from .forms import komentarForm, updateKasusForm
from .apps import NewsConfig
from django.apps import apps
from .views import tambahKomentar, kasus

# Create your tests here.

class UnitTestNews(TestCase):
    def test_url(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)

    def test_model_komentar(self):
        komentar.objects.create(komentar = "lekas sembuh")
        tst_komentar = komentar.objects.get(komentar = "lekas sembuh")
        self.assertEqual(str(tst_komentar), "lekas sembuh")

    def test_daftar_komentar(self):
        komentar.objects.create(komentar = "indonesia hebat")
        komentar.objects.create(komentar = "covid sangat meresahkan")
        counter = komentar.objects.all().count()
        self.assertEqual(counter, 2)

    def test_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news.html')

    def test_form_valid(self):
        data = {'komentar':"mari kita lawan covid"}
        komentar_form = komentarForm(data=data)
        self.assertTrue(komentar_form.is_valid())
        self.assertEqual(komentar_form.cleaned_data['komentar'],"mari kita lawan covid")

    def test_form_POST(self):
        # test_str = 'xyz'
        response_post = Client().post('/news/', {'komentar':"xyz"})
        self.assertEqual(response_post.status_code,200)
        # komentar_form = komentarForm(data={'komentar':test_str})
        # self.assertTrue(komentar_form.is_valid())
        # self.assertEqual(komentar_form.cleaned_data['komentar'],"xyz")

    def test_apps(self):
        self.assertEqual(NewsConfig.name, 'news')
        self.assertEqual(apps.get_app_config('news').name, 'news')