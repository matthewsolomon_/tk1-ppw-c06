from django import forms

class komentarForm(forms.Form):
    komentar = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control form-control-lg',
        # 'placeholder' : 'Berikan komentarmu terkait berita tersebut!',
        'type' : 'text',
        'required' : False,
    }))

class updateKasusForm(forms.Form):
    confirm_case = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control ',
        # 'placeholder' : 
        'type' : 'text',
        'required' : False,
    }))
    active_case = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control ',
        # 'placeholder' : 
        'type' : 'text',
        'required' : False,
    }))
    cured_case = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control ',
        # 'placeholder' : 
        'type' : 'text',
        'required' : False,
    }))
    region = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control ',
        # 'placeholder' : 
        'type' : 'text',
        'required' : False,
    }))