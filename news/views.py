from django.shortcuts import render, redirect
from .forms import komentarForm, updateKasusForm
from .models import komentar

# Create your views here.
# def news(request):
#     return render(request, 'news.html')

# def jumlahkasus(request):
#     return render(request, 'jumlahkasus.html')

def tambahKomentar(request):
    if request.method == 'POST':
        form = komentarForm(request.POST)
        if form.is_valid():
            inpMasuk = komentar()
            inpMasuk.komentar = form.cleaned_data['komentar']
            inpMasuk.save()
        return redirect("/news")

    else:
            inpMasuk = komentar.objects.all()
            form = komentarForm()
            response = {'inpMasuk':inpMasuk, 'form': form}
            return render(request, 'news.html', response)

def kasus(request):
    if request.method == 'POST':
        form2 = updateKasusForm(request.POST)
        return redirect("/detail")
    else:
        form2 = updateKasusForm()
        response = {'form2' : form2}
        return render(request, 'jumlahkasus.html', response)