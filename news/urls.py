
from django.urls import path

from . import views

app_name = 'news'

urlpatterns = [
    path('', views.tambahKomentar, name='news'),
    # path('Detail/', views.jumlahkasus, name='jumlahkasus'),
    path('detail', views.kasus, name='kasus'),

]