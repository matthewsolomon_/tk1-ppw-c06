
from django.urls import path

from . import views

app_name = 'donasi'

urlpatterns = [
    path('', views.index, name='index'),
]