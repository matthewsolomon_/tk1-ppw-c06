
from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('penggunaan-masker/', views.penggunaan_masker, name='masker'),
    path('tips-trick/', views.tips_trick, name='tips-trick')
]