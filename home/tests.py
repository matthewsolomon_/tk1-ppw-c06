from django.test import TestCase
from .forms import TipsTrickForm
from .models import TipsTrick

# Create your tests here.
class TipsTrickHTMLTest(TestCase):
    def test_text_exist(self):
        response = self.client.get('/tips-trick/')

        self.assertIn('Tips', response.content.decode())
        self.assertIn('Nama:', response.content.decode())
        self.assertIn('Tips', response.content.decode())

    def test_submit_button(self):
        response = self.client.get('/tips-trick/')

        self.assertIn('Submit', response.content.decode())

    def test_form(self):
        response = self.client.get('/tips-trick/')
        
        self.assertIn('<form', response.content.decode())
        self.assertIn('<input', response.content.decode())

class TipsTrickViewsTest(TestCase):
    def test_form_valid(self):
        response = self.client.post('/tips-trick/', data={
            'name': 'Qonita',
            'message': 'Pake masker',
        })
        self.assertTrue(TipsTrick.objects.filter(name="Qonita").exists())
