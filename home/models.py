from django.db import models

# Create your models here.
class TipsTrick(models.Model):
    name = models.CharField(max_length=50, blank=False)
    message = models.CharField(max_length=300, blank=False)