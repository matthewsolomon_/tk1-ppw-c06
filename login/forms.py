from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

class RegistrationForm(UserCreationForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={
        "class" : "form-control mb-3",
        "placeholder" : "Username"
    }))

    password1 = forms.CharField(label="Password", widget=forms.TextInput(attrs={
        "class" : "form-control mb-3",
        "placeholder" : "Password",
        "type" : "password"
    }))

    password2 = forms.CharField(label="Confirm Password*", widget=forms.TextInput(attrs={
        "class" : "form-control mb-3",
        "placeholder" : "9 digit, min 5 letters",
        "type" : "password"
    }))

    class Meta:
        model = User
        fields = ["username", "password1", "password2"]

class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={
        "class" : "form-control mb-3",
        "placeholder" : "Username"
    }))

    password = forms.CharField(label="Password", widget=forms.TextInput(attrs={
        "class" : "form-control mb-3",
        "placeholder" : "Password",
        "type" : "password"
    }))

    class Meta:
        fields = "__all__"

