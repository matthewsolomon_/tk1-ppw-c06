
from django.urls import path

from . import views

app_name = 'forum'

urlpatterns = [
    path('', views.index, name='index'),
    path('newpost/', views.new_post, name='new_post'),
    path('<slug:post>/', views.post_single, name='post_single'),
]
